import sys
import json

#Tweet Sentiments

def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
    scores = readsentfile(sent_file)
    tweets = readtweets(tweet_file)
    calculatesentiment(scores, tweets)

def calculatesentiment(scores, tweets):
    for tweet in tweets:
        score = 0
        words = tweet.split()
        for word in words:
            if word in scores:
                score += scores[word]
        print score

def readsentfile(afile):
	  scores = {}
	  for line in afile.readlines():
	  	term, score = line.split("\t")
	  	scores[term] = int(score)
	  return scores  	

def readtweets(myfile):
    tweets = []
    for line in myfile.readlines():
        linedict = json.loads(line)
        if 'text' in linedict:
            tweets.append(linedict['text'].encode("utf-8"))
    return tweets        

if __name__ == '__main__':
    main()
